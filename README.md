# Gestion de stages

Cette application a été créée par **Lilian LABROSSE** et **Loïc BOIVIN** dans le cadre de l'UE Informatique répartie.

## Prérequis

Lancer le script sql *bdd_geststages.sql* pour créer la base de données. Il vous faudra également donner tous les droits sur cette base à un utilisateur avec pour login *usergs* et pour password *mdpGS* pour pouvoir utiliser l'application.

## Fonctionnalités

Pour essayer l'application vous pourrez utiliser les comptes suivants :
|type de compte|login   |password|
|--------------|--------|--------|
|professeur    |merlot  | secret |
|étudiant      |benpas01|benpas01|

En tant qu'étudiant vous pourrez regarder les détails des différentes entreprises et des autres étudiants. Vous pourrez également vous inscrire à un stage.
En tant que professeur vous pourrez également ajouter, modifier et supprimer un étudiant ou une entreprise.