package fr.epul.gestionstages.service;

import fr.epul.gestionstages.entity.EtudiantEntity;
import fr.epul.gestionstages.repository.EtudiantRepository;
import fr.epul.gestionstages.utils.HashPassword;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EtudiantService{

    private EtudiantRepository etudiantRepository;

    public EtudiantService(EtudiantRepository etudiantRepository) {
        this.etudiantRepository = etudiantRepository;
    }

    public List<EtudiantEntity> getAllEtudiants() {
        return etudiantRepository.findAll();
    }

    public EtudiantEntity saveEtudiant(EtudiantEntity etudiant) {
        String hashedPassword = new HashPassword().hashPassword(etudiant.getMdp());
        etudiant.setMdp(hashedPassword);
        return etudiantRepository.save(etudiant);
    }

    public EtudiantEntity getEtudiantById(Long id) {
        return etudiantRepository.findById(id).get();
    }

    public EtudiantEntity updateEtudiant(EtudiantEntity etudiant) {
        String hashedPassword = new HashPassword().hashPassword(etudiant.getMdp());
        etudiant.setMdp(hashedPassword);
        return etudiantRepository.save(etudiant);
    }

    public void deleteEtudiantById(Long id) {etudiantRepository.deleteById(id); }

    public boolean authEtudiant (String login, String password, HttpSession session) {
        String hashedPassword = new HashPassword().hashPassword(password);
        EtudiantEntity etudiant = etudiantRepository.findEtudiantEntityByLoginAndMdp(login, hashedPassword);
        EtudiantEntity etudiantNoHash = etudiantRepository.findEtudiantEntityByLoginAndMdp(login, password);
        if (etudiant != null) {
            session.setAttribute("userName", etudiant.getPrenomEtudiant());
            session.setAttribute("userId", etudiant.getNumEtudiant());
            session.setAttribute("userRole", "etudiant");
            return true;
        }
        if (etudiantNoHash != null) {
            session.setAttribute("userName", etudiantNoHash.getPrenomEtudiant());
            session.setAttribute("userId", etudiantNoHash.getNumEtudiant());
            session.setAttribute("userRole", "etudiant");
            return true;
        }
        return false;
    }
}