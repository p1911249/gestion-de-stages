package fr.epul.gestionstages.service;

import fr.epul.gestionstages.entity.EntrepriseEntity;
import fr.epul.gestionstages.entity.StageEntity;
import fr.epul.gestionstages.repository.EntrepriseRepository;
import fr.epul.gestionstages.repository.StageRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntrepriseService{

    private EntrepriseRepository entrepriseRepository;

    public EntrepriseService(EntrepriseRepository entrepriseRepository) {
        this.entrepriseRepository = entrepriseRepository;
    }

    public List<EntrepriseEntity> getAllCompanies() {
        return entrepriseRepository.findAll();
    }

    public EntrepriseEntity saveEntreprise(EntrepriseEntity entreprise) {
        return entrepriseRepository.save(entreprise);
    }

    public EntrepriseEntity getEntrepriseById(Long id) {
        return entrepriseRepository.findById(id).get();
    }

    public EntrepriseEntity updateEntreprise(EntrepriseEntity entreprise) {
        return entrepriseRepository.save(entreprise);
    }

    public void deleteEntrepriseById(Long id) {entrepriseRepository.deleteById(id); }
}
