package fr.epul.gestionstages.service;

import fr.epul.gestionstages.entity.ProfesseurEntity;
import fr.epul.gestionstages.repository.ProfesseurRepository;
import fr.epul.gestionstages.utils.HashPassword;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfesseurService {

    private ProfesseurRepository professeurRepository;

    public ProfesseurService(ProfesseurRepository professeurRepository) {
        this.professeurRepository = professeurRepository;
    }

    public boolean authProfesseur (String login, String password, HttpSession session) {
        String hashedPassword = new HashPassword().hashPassword(password);
        ProfesseurEntity professeur = professeurRepository.findProfesseurEntityByLoginAndMdp(login, password);
        ProfesseurEntity professeurNoHash = professeurRepository.findProfesseurEntityByLoginAndMdp(login, hashedPassword);
        if (professeur != null) {
            session.setAttribute("userName", professeur.getNomProf());
            session.setAttribute("userId", professeur.getNumProf());
            session.setAttribute("userRole", "prof");
            return true;
        }
        if (professeurNoHash != null) {
            session.setAttribute("userName", professeurNoHash.getNomProf());
            session.setAttribute("userId", professeurNoHash.getNumProf());
            session.setAttribute("userRole", "prof");
            return true;
        }
        return false;
    }

    public List<ProfesseurEntity> getAllProfesseurs() {
        return professeurRepository.findAll();
    }
}
