package fr.epul.gestionstages.service;

import fr.epul.gestionstages.entity.StageEntity;
import fr.epul.gestionstages.repository.StageRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StageService {

    private StageRepository stageRepository;

    public StageService(StageRepository stageRepository) {
        this.stageRepository = stageRepository;
    }

    public StageEntity saveStage(StageEntity stage) {
        return stageRepository.save(stage);
    }

    public List<StageEntity> getStagesByEntrepriseId(Long id) {
        return stageRepository.findStageEntitiesByEntrepriseNumEntreprise(Math.toIntExact(id));
    }

    public List<StageEntity> getStagesByEtudiantId(Long id) {
        return stageRepository.findStageEntitiesByEtudiant_NumEtudiant(Math.toIntExact(id));
    }
}
