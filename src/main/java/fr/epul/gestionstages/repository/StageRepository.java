package fr.epul.gestionstages.repository;


import fr.epul.gestionstages.entity.StageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StageRepository extends JpaRepository<StageEntity, Long> {

    List<StageEntity> findStageEntitiesByEntrepriseNumEntreprise(int entreprise_numEntreprise);

    List<StageEntity> findStageEntitiesByEtudiant_NumEtudiant(int entreprise_numEntreprise);
}
