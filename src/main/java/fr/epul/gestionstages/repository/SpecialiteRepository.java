package fr.epul.gestionstages.repository;

import fr.epul.gestionstages.entity.ProfesseurEntity;
import fr.epul.gestionstages.entity.SpecialiteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpecialiteRepository extends JpaRepository<SpecialiteEntity, Integer> {
    SpecialiteEntity findSpecialiteEntityByNumSpec(int numSpec);
}
