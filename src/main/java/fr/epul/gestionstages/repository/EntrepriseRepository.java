package fr.epul.gestionstages.repository;

import fr.epul.gestionstages.entity.EntrepriseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntrepriseRepository extends JpaRepository<EntrepriseEntity, Long> {
}
