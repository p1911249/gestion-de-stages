package fr.epul.gestionstages.repository;

import fr.epul.gestionstages.entity.EtudiantEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EtudiantRepository extends JpaRepository<EtudiantEntity, Long> {
    EtudiantEntity findEtudiantEntityByLoginAndMdp(String login, String mdp);

}
