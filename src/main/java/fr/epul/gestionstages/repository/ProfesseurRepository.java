package fr.epul.gestionstages.repository;

import fr.epul.gestionstages.entity.EtudiantEntity;
import fr.epul.gestionstages.entity.ProfesseurEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfesseurRepository extends JpaRepository<ProfesseurEntity, Long> {
    ProfesseurEntity findProfesseurEntityByLoginAndMdp(String login, String pwd);
}
