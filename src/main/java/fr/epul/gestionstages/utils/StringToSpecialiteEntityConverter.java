package fr.epul.gestionstages.utils;

import fr.epul.gestionstages.entity.SpecialiteEntity;
import fr.epul.gestionstages.repository.SpecialiteRepository;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToSpecialiteEntityConverter
        implements Converter<String, SpecialiteEntity> {

    private SpecialiteRepository specialiteRepository;

    StringToSpecialiteEntityConverter(SpecialiteRepository specialiteRepository){
        this.specialiteRepository = specialiteRepository;
    }

    @Override
    public SpecialiteEntity convert(String source) {
        return specialiteRepository.findSpecialiteEntityByNumSpec(Integer.parseInt(source));
    }
}
