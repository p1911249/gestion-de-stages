package fr.epul.gestionstages.utils;

import java.util.logging.Logger;

public class HashPassword {
    public String hashPassword(String password) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] byteData = md.digest(password.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; ++i) {
                sb.append(Integer.toHexString((byteData[i] & 0xFF) | 0x100).substring(1));
            }
            String sbReduced = sb.toString().substring(0, 29);
            return sbReduced;
        } catch (java.security.NoSuchAlgorithmException e) {
            Logger.getLogger("MD5").severe(e.getMessage());
            return null;        }
    }
}
