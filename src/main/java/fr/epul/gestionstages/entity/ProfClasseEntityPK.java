package fr.epul.gestionstages.entity;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.io.Serializable;

public class ProfClasseEntityPK implements Serializable {
    @Column(name = "num_prof")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int numProf;
    @Column(name = "num_classe")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int numClasse;

    public int getNumProf() {
        return numProf;
    }

    public void setNumProf(int numProf) {
        this.numProf = numProf;
    }

    public int getNumClasse() {
        return numClasse;
    }

    public void setNumClasse(int numClasse) {
        this.numClasse = numClasse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfClasseEntityPK that = (ProfClasseEntityPK) o;

        if (numProf != that.numProf) return false;
        if (numClasse != that.numClasse) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = numProf;
        result = 31 * result + numClasse;
        return result;
    }
}
