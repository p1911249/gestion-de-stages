package fr.epul.gestionstages.entity;

import jakarta.persistence.*;

import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "etudiant", schema = "bdd_geststages", catalog = "")
public class EtudiantEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "num_etudiant")
    private int numEtudiant;
    @Basic
    @Column(name = "nom_etudiant")
    private String nomEtudiant;
    @Basic
    @Column(name = "prenom_etudiant")
    private String prenomEtudiant;
    @Basic
    @Column(name = "login")
    private String login;
    @Basic
    @Column(name = "mdp")
    private String mdp;
    @Basic
    @Column(name = "num_classe")
    private int numClasse;
    @OneToMany(mappedBy = "etudiant")
    private List<StageEntity> stages;

    public StageEntity getStage() {
        return stages.size() == 0 ? null : stages.get(0);
    }

    public int getNumEtudiant() {
        return numEtudiant;
    }

    public void setNumEtudiant(int numEtudiant) {
        this.numEtudiant = numEtudiant;
    }

    public String getNomEtudiant() {
        return nomEtudiant;
    }

    public void setNomEtudiant(String nomEtudiant) {
        this.nomEtudiant = nomEtudiant;
    }

    public String getPrenomEtudiant() {
        return prenomEtudiant;
    }

    public void setPrenomEtudiant(String prenomEtudiant) {
        this.prenomEtudiant = prenomEtudiant;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public int getNumClasse() {
        return numClasse;
    }

    public void setNumClasse(int numClasse) {
        this.numClasse = numClasse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EtudiantEntity that = (EtudiantEntity) o;

        if (numEtudiant != that.numEtudiant) return false;
        if (numClasse != that.numClasse) return false;
        if (nomEtudiant != null ? !nomEtudiant.equals(that.nomEtudiant) : that.nomEtudiant != null) return false;
        if (prenomEtudiant != null ? !prenomEtudiant.equals(that.prenomEtudiant) : that.prenomEtudiant != null)
            return false;
        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (mdp != null ? !mdp.equals(that.mdp) : that.mdp != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = numEtudiant;
        result = 31 * result + (nomEtudiant != null ? nomEtudiant.hashCode() : 0);
        result = 31 * result + (prenomEtudiant != null ? prenomEtudiant.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (mdp != null ? mdp.hashCode() : 0);
        result = 31 * result + numClasse;
        return result;
    }
}
