package fr.epul.gestionstages.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "spec_entreprise", schema = "bdd_geststages", catalog = "")
@IdClass(SpecEntrepriseEntityPK.class)
public class SpecEntrepriseEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "num_entreprise")
    private int numEntreprise;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "num_spec")
    private int numSpec;

    public int getNumEntreprise() {
        return numEntreprise;
    }

    public void setNumEntreprise(int numEntreprise) {
        this.numEntreprise = numEntreprise;
    }

    public int getNumSpec() {
        return numSpec;
    }

    public void setNumSpec(int numSpec) {
        this.numSpec = numSpec;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpecEntrepriseEntity that = (SpecEntrepriseEntity) o;

        if (numEntreprise != that.numEntreprise) return false;
        if (numSpec != that.numSpec) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = numEntreprise;
        result = 31 * result + numSpec;
        return result;
    }
}
