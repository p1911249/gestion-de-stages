package fr.epul.gestionstages.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "prof_classe", schema = "bdd_geststages", catalog = "")
@IdClass(ProfClasseEntityPK.class)
public class ProfClasseEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "num_prof")
    private int numProf;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "num_classe")
    private int numClasse;
    @Basic
    @Column(name = "est_prof_principal")
    private byte estProfPrincipal;

    public int getNumProf() {
        return numProf;
    }

    public void setNumProf(int numProf) {
        this.numProf = numProf;
    }

    public int getNumClasse() {
        return numClasse;
    }

    public void setNumClasse(int numClasse) {
        this.numClasse = numClasse;
    }

    public byte getEstProfPrincipal() {
        return estProfPrincipal;
    }

    public void setEstProfPrincipal(byte estProfPrincipal) {
        this.estProfPrincipal = estProfPrincipal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfClasseEntity that = (ProfClasseEntity) o;

        if (numProf != that.numProf) return false;
        if (numClasse != that.numClasse) return false;
        if (estProfPrincipal != that.estProfPrincipal) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = numProf;
        result = 31 * result + numClasse;
        result = 31 * result + (int) estProfPrincipal;
        return result;
    }
}
