package fr.epul.gestionstages.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "entreprise", schema = "bdd_geststages", catalog = "")
public class EntrepriseEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "num_entreprise")
    private int numEntreprise;
    @Basic
    @Column(name = "raison_sociale")
    private String raisonSociale;
    @Basic
    @Column(name = "nom_contact")
    private String nomContact;
    @Basic
    @Column(name = "nom_resp")
    private String nomResp;
    @Basic
    @Column(name = "rue_entreprise")
    private String rueEntreprise;
    @Basic
    @Column(name = "cp_entreprise")
    private Integer cpEntreprise;
    @Basic
    @Column(name = "ville_entreprise")
    private String villeEntreprise;
    @Basic
    @Column(name = "tel_entreprise")
    private String telEntreprise;
    @Basic
    @Column(name = "fax_entreprise")
    private String faxEntreprise;
    @Basic
    @Column(name = "email")
    private String email;
    @Basic
    @Column(name = "observation")
    private String observation;
    @Basic
    @Column(name = "site_entreprise")
    private String siteEntreprise;
    @Column(name = "niveau")
    private String niveau;
    @Basic
    @Column(name = "en_activite")
    private byte enActivite;

    @JoinTable(
            name = "spec_entreprise",
            joinColumns = @JoinColumn(
                    name = "num_entreprise",
                    referencedColumnName = "num_entreprise"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "num_spec",
                    referencedColumnName = "num_spec"
            )
    )
    @ManyToOne
    private SpecialiteEntity specialite;

    public EntrepriseEntity() {
    }

    public SpecialiteEntity getSpecialite() {
        return specialite;
    }

    public void setSpecialite(SpecialiteEntity specialite) {
        this.specialite = specialite;
    }

    public int getNumEntreprise() {
        return numEntreprise;
    }

    public void setNumEntreprise(int numEntreprise) {
        this.numEntreprise = numEntreprise;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public String getNomContact() {
        return nomContact;
    }

    public void setNomContact(String nomContact) {
        this.nomContact = nomContact;
    }

    public String getNomResp() {
        return nomResp;
    }

    public void setNomResp(String nomResp) {
        this.nomResp = nomResp;
    }

    public String getRueEntreprise() {
        return rueEntreprise;
    }

    public void setRueEntreprise(String rueEntreprise) {
        this.rueEntreprise = rueEntreprise;
    }

    public Integer getCpEntreprise() {
        return cpEntreprise;
    }

    public void setCpEntreprise(Integer cpEntreprise) {
        this.cpEntreprise = cpEntreprise;
    }

    public String getVilleEntreprise() {
        return villeEntreprise;
    }

    public void setVilleEntreprise(String villeEntreprise) {
        this.villeEntreprise = villeEntreprise;
    }

    public String getTelEntreprise() {
        return telEntreprise;
    }

    public void setTelEntreprise(String telEntreprise) {
        this.telEntreprise = telEntreprise;
    }

    public String getFaxEntreprise() {
        return faxEntreprise;
    }

    public void setFaxEntreprise(String faxEntreprise) {
        this.faxEntreprise = faxEntreprise;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getSiteEntreprise() {
        return siteEntreprise;
    }

    public void setSiteEntreprise(String siteEntreprise) {
        this.siteEntreprise = siteEntreprise;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public byte getEnActivite() {
        return enActivite;
    }

    public void setEnActivite(byte enActivite) {
        this.enActivite = enActivite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntrepriseEntity that = (EntrepriseEntity) o;

        if (numEntreprise != that.numEntreprise) return false;
        if (enActivite != that.enActivite) return false;
        if (raisonSociale != null ? !raisonSociale.equals(that.raisonSociale) : that.raisonSociale != null)
            return false;
        if (nomContact != null ? !nomContact.equals(that.nomContact) : that.nomContact != null) return false;
        if (nomResp != null ? !nomResp.equals(that.nomResp) : that.nomResp != null) return false;
        if (rueEntreprise != null ? !rueEntreprise.equals(that.rueEntreprise) : that.rueEntreprise != null)
            return false;
        if (cpEntreprise != null ? !cpEntreprise.equals(that.cpEntreprise) : that.cpEntreprise != null) return false;
        if (villeEntreprise != null ? !villeEntreprise.equals(that.villeEntreprise) : that.villeEntreprise != null)
            return false;
        if (telEntreprise != null ? !telEntreprise.equals(that.telEntreprise) : that.telEntreprise != null)
            return false;
        if (faxEntreprise != null ? !faxEntreprise.equals(that.faxEntreprise) : that.faxEntreprise != null)
            return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (observation != null ? !observation.equals(that.observation) : that.observation != null) return false;
        if (siteEntreprise != null ? !siteEntreprise.equals(that.siteEntreprise) : that.siteEntreprise != null)
            return false;
        if (niveau != null ? !niveau.equals(that.niveau) : that.niveau != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = numEntreprise;
        result = 31 * result + (raisonSociale != null ? raisonSociale.hashCode() : 0);
        result = 31 * result + (nomContact != null ? nomContact.hashCode() : 0);
        result = 31 * result + (nomResp != null ? nomResp.hashCode() : 0);
        result = 31 * result + (rueEntreprise != null ? rueEntreprise.hashCode() : 0);
        result = 31 * result + (cpEntreprise != null ? cpEntreprise.hashCode() : 0);
        result = 31 * result + (villeEntreprise != null ? villeEntreprise.hashCode() : 0);
        result = 31 * result + (telEntreprise != null ? telEntreprise.hashCode() : 0);
        result = 31 * result + (faxEntreprise != null ? faxEntreprise.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (observation != null ? observation.hashCode() : 0);
        result = 31 * result + (siteEntreprise != null ? siteEntreprise.hashCode() : 0);
        result = 31 * result + (niveau != null ? niveau.hashCode() : 0);
        result = 31 * result + (int) enActivite;
        return result;
    }
}
