package fr.epul.gestionstages.entity;

import jakarta.persistence.*;

import java.sql.Timestamp;
import java.time.LocalDate;

@Entity
@Table(name = "stage", schema = "bdd_geststages", catalog = "")
public class StageEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "num_stage")
    private int numStage;
    @Basic
    @Column(name = "debut_stage")
    private LocalDate debutStage;
    @Basic
    @Column(name = "fin_stage")
    private LocalDate finStage;
    @Basic
    @Column(name = "type_stage")
    private String typeStage;
    @Basic
    @Column(name = "desc_projet")
    private String descProjet;
    @Basic
    @Column(name = "observation_stage")
    private String observationStage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "num_etudiant")
    private EtudiantEntity etudiant;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "num_prof")
    private ProfesseurEntity prof;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "num_entreprise")
    private EntrepriseEntity entreprise;

    public int getNumStage() {
        return numStage;
    }

    public void setNumStage(int numStage) {
        this.numStage = numStage;
    }

    public LocalDate getDebutStage() {
        return debutStage;
    }

    public void setDebutStage(LocalDate debutStage) {
        this.debutStage = debutStage;
    }

    public LocalDate getFinStage() {
        return finStage;
    }

    public void setFinStage(LocalDate finStage) {
        this.finStage = finStage;
    }

    public String getTypeStage() {
        return typeStage;
    }

    public void setTypeStage(String typeStage) {
        this.typeStage = typeStage;
    }

    public String getDescProjet() {
        return descProjet;
    }

    public void setDescProjet(String descProjet) {
        this.descProjet = descProjet;
    }

    public String getObservationStage() {
        return observationStage;
    }

    public void setObservationStage(String observationStage) {
        this.observationStage = observationStage;
    }

    public EtudiantEntity getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(EtudiantEntity etudiant) {
        this.etudiant = etudiant;
    }

    public ProfesseurEntity getProf() {
        return prof;
    }

    public void setProf(ProfesseurEntity prof) {
        this.prof = prof;
    }

    public EntrepriseEntity getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(EntrepriseEntity entreprise) {
        this.entreprise = entreprise;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StageEntity that = (StageEntity) o;

        if (numStage != that.numStage) return false;
        if (etudiant != that.etudiant) return false;
        if (prof != that.prof) return false;
        if (entreprise != that.entreprise) return false;
        if (debutStage != null ? !debutStage.equals(that.debutStage) : that.debutStage != null) return false;
        if (finStage != null ? !finStage.equals(that.finStage) : that.finStage != null) return false;
        if (typeStage != null ? !typeStage.equals(that.typeStage) : that.typeStage != null) return false;
        if (descProjet != null ? !descProjet.equals(that.descProjet) : that.descProjet != null) return false;
        if (observationStage != null ? !observationStage.equals(that.observationStage) : that.observationStage != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = numStage;
        result = 31 * result + (debutStage != null ? debutStage.hashCode() : 0);
        result = 31 * result + (finStage != null ? finStage.hashCode() : 0);
        result = 31 * result + (typeStage != null ? typeStage.hashCode() : 0);
        result = 31 * result + (descProjet != null ? descProjet.hashCode() : 0);
        result = 31 * result + (observationStage != null ? observationStage.hashCode() : 0);
        result = 31 * result + etudiant.getNumEtudiant();
        result = 31 * result + prof.getNumProf();
        result = 31 * result + entreprise.getNumEntreprise();
        return result;
    }
}
