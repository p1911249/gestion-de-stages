package fr.epul.gestionstages.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "specialite", schema = "bdd_geststages", catalog = "")
public class SpecialiteEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "num_spec")
    private int numSpec;
    @Basic
    @Column(name = "libelle")
    private String libelle;

    public int getNumSpec() {
        return numSpec;
    }

    public void setNumSpec(int numSpec) {
        this.numSpec = numSpec;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpecialiteEntity that = (SpecialiteEntity) o;

        if (numSpec != that.numSpec) return false;
        if (libelle != null ? !libelle.equals(that.libelle) : that.libelle != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = numSpec;
        result = 31 * result + (libelle != null ? libelle.hashCode() : 0);
        return result;
    }
}
