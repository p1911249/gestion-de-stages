package fr.epul.gestionstages.entity;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.io.Serializable;

public class SpecEntrepriseEntityPK implements Serializable {
    @Column(name = "num_entreprise")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int numEntreprise;
    @Column(name = "num_spec")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int numSpec;

    public int getNumEntreprise() {
        return numEntreprise;
    }

    public void setNumEntreprise(int numEntreprise) {
        this.numEntreprise = numEntreprise;
    }

    public int getNumSpec() {
        return numSpec;
    }

    public void setNumSpec(int numSpec) {
        this.numSpec = numSpec;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpecEntrepriseEntityPK that = (SpecEntrepriseEntityPK) o;

        if (numEntreprise != that.numEntreprise) return false;
        if (numSpec != that.numSpec) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = numEntreprise;
        result = 31 * result + numSpec;
        return result;
    }
}
