package fr.epul.gestionstages.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "professeur", schema = "bdd_geststages", catalog = "")
public class ProfesseurEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "num_prof")
    private int numProf;
    @Basic
    @Column(name = "nom_prof")
    private String nomProf;
    @Basic
    @Column(name = "prenom_prof")
    private String prenomProf;
    @Basic
    @Column(name = "login")
    private String login;
    @Basic
    @Column(name = "mdp")
    private String mdp;
    @Basic
    @Column(name = "email")
    private String email;

    public int getNumProf() {
        return numProf;
    }

    public void setNumProf(int numProf) {
        this.numProf = numProf;
    }

    public String getNomProf() {
        return nomProf;
    }

    public void setNomProf(String nomProf) {
        this.nomProf = nomProf;
    }

    public String getPrenomProf() {
        return prenomProf;
    }

    public void setPrenomProf(String prenomProf) {
        this.prenomProf = prenomProf;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfesseurEntity that = (ProfesseurEntity) o;

        if (numProf != that.numProf) return false;
        if (nomProf != null ? !nomProf.equals(that.nomProf) : that.nomProf != null) return false;
        if (prenomProf != null ? !prenomProf.equals(that.prenomProf) : that.prenomProf != null) return false;
        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (mdp != null ? !mdp.equals(that.mdp) : that.mdp != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = numProf;
        result = 31 * result + (nomProf != null ? nomProf.hashCode() : 0);
        result = 31 * result + (prenomProf != null ? prenomProf.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (mdp != null ? mdp.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }
}
