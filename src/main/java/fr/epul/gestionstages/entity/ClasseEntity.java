package fr.epul.gestionstages.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "classe", schema = "bdd_geststages", catalog = "")
public class ClasseEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "num_classe")
    private int numClasse;
    @Basic
    @Column(name = "nom_classe")
    private String nomClasse;

    public int getNumClasse() {
        return numClasse;
    }

    public void setNumClasse(int numClasse) {
        this.numClasse = numClasse;
    }

    public String getNomClasse() {
        return nomClasse;
    }

    public void setNomClasse(String nomClasse) {
        this.nomClasse = nomClasse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClasseEntity that = (ClasseEntity) o;

        if (numClasse != that.numClasse) return false;
        if (nomClasse != null ? !nomClasse.equals(that.nomClasse) : that.nomClasse != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = numClasse;
        result = 31 * result + (nomClasse != null ? nomClasse.hashCode() : 0);
        return result;
    }
}
