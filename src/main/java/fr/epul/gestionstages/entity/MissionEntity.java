package fr.epul.gestionstages.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "mission", schema = "bdd_geststages", catalog = "")
public class MissionEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "num_mission")
    private int numMission;
    @Basic
    @Column(name = "libelle")
    private String libelle;
    @Basic
    @Column(name = "num_stage")
    private int numStage;

    public int getNumMission() {
        return numMission;
    }

    public void setNumMission(int numMission) {
        this.numMission = numMission;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getNumStage() {
        return numStage;
    }

    public void setNumStage(int numStage) {
        this.numStage = numStage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MissionEntity that = (MissionEntity) o;

        if (numMission != that.numMission) return false;
        if (numStage != that.numStage) return false;
        if (libelle != null ? !libelle.equals(that.libelle) : that.libelle != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = numMission;
        result = 31 * result + (libelle != null ? libelle.hashCode() : 0);
        result = 31 * result + numStage;
        return result;
    }
}
