package fr.epul.gestionstages;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionStagesApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(GestionStagesApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
