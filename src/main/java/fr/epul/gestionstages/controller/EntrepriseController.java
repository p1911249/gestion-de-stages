package fr.epul.gestionstages.controller;

import fr.epul.gestionstages.entity.EntrepriseEntity;
import fr.epul.gestionstages.service.EntrepriseService;
import fr.epul.gestionstages.service.StageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class EntrepriseController {

    private EntrepriseService entrepriseService;
    private StageService stageService;

    public EntrepriseController(EntrepriseService entrepriseService,
                                StageService stageService) {
        this.entrepriseService = entrepriseService;
        this.stageService = stageService;
    }

    @GetMapping("/companies")
    public String listEntreprises(Model model) {
        model.addAttribute("entreprises", entrepriseService.getAllCompanies());
        return "list_companies";
    }

    @GetMapping("/companies/edit/{id}")
    public String editEntrepriseForm(@PathVariable Long id, Model model){
        model.addAttribute("company", entrepriseService.getEntrepriseById(id));
        return "edit_company";
    }

    @GetMapping("/companies/new")
    public String createEntrepriseForm(Model model){
        EntrepriseEntity company = new EntrepriseEntity();
        model.addAttribute("company", company);
        return "create_company";
    }

    @GetMapping("/companies/{id}")
    public String showEntreprise(@PathVariable Long id, Model model){
        model.addAttribute("company", entrepriseService.getEntrepriseById(id));
        model.addAttribute("stages", stageService.getStagesByEntrepriseId(id));
        return "show_company";
    }

    @PostMapping("/companies")
    public String saveEntreprises(@ModelAttribute("company") EntrepriseEntity company) {
        entrepriseService.saveEntreprise(company);
        return "redirect:/companies";
    }

    @PostMapping("/companies/{id}")
    public String udpateEntreprise(@PathVariable Long id, @ModelAttribute("company") EntrepriseEntity company, Model model) {
        EntrepriseEntity currentEntreprise = entrepriseService.getEntrepriseById(id);
        currentEntreprise.setRaisonSociale(company.getRaisonSociale());
        currentEntreprise.setNomContact(company.getNomContact());
        currentEntreprise.setNomResp(company.getNomResp());
        currentEntreprise.setRueEntreprise(company.getRueEntreprise());
        currentEntreprise.setCpEntreprise(company.getCpEntreprise());
        currentEntreprise.setVilleEntreprise(company.getVilleEntreprise());
        currentEntreprise.setTelEntreprise(company.getTelEntreprise());
        currentEntreprise.setFaxEntreprise(company.getFaxEntreprise());
        currentEntreprise.setEmail(company.getEmail());
        currentEntreprise.setObservation(company.getObservation());
        currentEntreprise.setNiveau(company.getNiveau());

        entrepriseService.updateEntreprise(currentEntreprise);
        return "redirect:/companies";
    }

    @GetMapping("/companies/delete/{id}")
    public String deleteEntreprise(@PathVariable Long id) {
        entrepriseService.deleteEntrepriseById(id);
        return "redirect:/companies";
    }
}
