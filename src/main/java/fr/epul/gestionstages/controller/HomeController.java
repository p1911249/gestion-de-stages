package fr.epul.gestionstages.controller;

import ch.qos.logback.core.encoder.EchoEncoder;
import fr.epul.gestionstages.service.EtudiantService;
import fr.epul.gestionstages.service.ProfesseurService;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class HomeController {

    private EtudiantService etudiantService;
    private ProfesseurService professeurService;



    public HomeController(EtudiantService etudiantService, ProfesseurService professeurService) {
        this.etudiantService = etudiantService;
        this.professeurService = professeurService;
    }

    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/login")
    public String loginPage() {
        return "login_form";
    }

    @GetMapping("/logout")
    public String logout(HttpSession session) {
        session.setAttribute("userName", "");
        session.setAttribute("userRole", "");
        return "redirect:/";
    }

    @PostMapping("/loginStudent")
    public String loginEtudiant(@RequestParam(name = "login", required = false) String login, @RequestParam(name = "password", required = false) String password, HttpSession session, RedirectAttributes redirectAttrs){
        if(etudiantService.authEtudiant(login, password, session)){
            return "redirect:/";
        } else {
            redirectAttrs.addFlashAttribute("errorMessageEtudiant", "Nom d'utilisateur ou mot de passe incorrect");
            return "redirect:/login";
        }
    }

    @PostMapping("/loginProf")
    public String loginProfesseur(@RequestParam(name = "login", required = false) String login, @RequestParam(name = "password", required = false) String password, HttpSession session, RedirectAttributes redirectAttrs){
        if(professeurService.authProfesseur(login, password, session)) {
            return "redirect:/";
        } else {
            redirectAttrs.addFlashAttribute("errorMessageProf", "Nom d'utilisateur ou mot de passe incorrect");
            return "redirect:/login";
        }
    }

    @GetMapping("/help")
    public String help() {
        return "help";
    }
}
