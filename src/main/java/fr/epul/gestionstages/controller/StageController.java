package fr.epul.gestionstages.controller;

import fr.epul.gestionstages.entity.EntrepriseEntity;
import fr.epul.gestionstages.entity.EtudiantEntity;
import fr.epul.gestionstages.entity.ProfesseurEntity;
import fr.epul.gestionstages.entity.StageEntity;
import fr.epul.gestionstages.service.EntrepriseService;
import fr.epul.gestionstages.service.EtudiantService;
import fr.epul.gestionstages.service.ProfesseurService;
import fr.epul.gestionstages.service.StageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class StageController {

    private StageService stageService;
    private EtudiantService etudiantService;
    private EntrepriseService entrepriseService;
    private ProfesseurService professeurService;
    public StageController(StageService stageService,
                           EtudiantService etudiantService,
                           EntrepriseService entrepriseService,
                           ProfesseurService professeurService)
    {
        this.stageService = stageService;
        this.etudiantService = etudiantService;
        this.entrepriseService = entrepriseService;
        this.professeurService = professeurService;
    }

    @GetMapping("/stage/new")
    public String registration(Model model, @ModelAttribute("student") EtudiantEntity etudiant,
               @ModelAttribute("teacher") ProfesseurEntity professeur,
               @ModelAttribute("company") EntrepriseEntity entreprise)
    {
        StageEntity stage = new StageEntity();
        model.addAttribute("etudiants", etudiantService.getAllEtudiants());
        model.addAttribute("profs", professeurService.getAllProfesseurs());
        model.addAttribute("entreprises", entrepriseService.getAllCompanies());
        model.addAttribute("stage", stage);
        return "create_stage";
    }

    @PostMapping("/stages")
    public String saveStage(@ModelAttribute("stage") StageEntity stage) {
        stageService.saveStage(stage);
        return "redirect:/students";
    }
}
