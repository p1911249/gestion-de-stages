package fr.epul.gestionstages.controller;

import fr.epul.gestionstages.entity.EtudiantEntity;
import fr.epul.gestionstages.service.EtudiantService;
import fr.epul.gestionstages.service.StageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class EtudiantController {

    private EtudiantService etudiantService;
    private StageService stageService;

    public EtudiantController(EtudiantService etudiantService,
                              StageService stageService) {
        this.etudiantService = etudiantService;
        this.stageService = stageService;
    }

    @GetMapping("/students")
    public String listEtudiants(Model model) {
        model.addAttribute("students", etudiantService.getAllEtudiants());
        return "list_students";
    }

    @GetMapping("/students/edit/{id}")
    public String editEtudiantForm(@PathVariable Long id, Model model){
        model.addAttribute("student", etudiantService.getEtudiantById(id));
        return "edit_student";
    }

    @GetMapping("/students/new")
    public String createEtudiantForm(Model model){
        EtudiantEntity student = new EtudiantEntity();
        model.addAttribute("student", student);
        return "create_student";
    }

    @GetMapping("/students/{id}")
    public String showEtudiant(@PathVariable Long id, Model model){
        model.addAttribute("student", etudiantService.getEtudiantById(id));
        model.addAttribute("stages", stageService.getStagesByEtudiantId(id));
        return "show_student";
    }

    @PostMapping("/students")
    public String saveEtudiants(@ModelAttribute("student") EtudiantEntity student) {
        etudiantService.saveEtudiant(student);
        return "redirect:/students";
    }

    @PostMapping("/students/{id}")
    public String udpateEtudiant(@PathVariable Long id, @ModelAttribute("student") EtudiantEntity student) {
        EtudiantEntity currentEtudiant = etudiantService.getEtudiantById(id);
        currentEtudiant.setPrenomEtudiant(student.getPrenomEtudiant());
        currentEtudiant.setNomEtudiant(student.getNomEtudiant());
        currentEtudiant.setNumClasse(student.getNumClasse());

        etudiantService.updateEtudiant(currentEtudiant);
        return "redirect:/students";
    }

    @GetMapping("/students/delete/{id}")
    public String deleteEtudiant(@PathVariable Long id) {
        etudiantService.deleteEtudiantById(id);
        return "redirect:/students";
    }
}
