DROP DATABASE if exists `bdd_geststages`;
CREATE DATABASE  `bdd_geststages` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `bdd_geststages`;

-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: bdd_geststages
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE IF EXISTS `classe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `classe` (
  `num_classe` int NOT NULL AUTO_INCREMENT,
  `nom_classe` varchar(128) NOT NULL,
  PRIMARY KEY (`num_classe`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classe`
--

LOCK TABLES `classe` WRITE;
/*!40000 ALTER TABLE `classe` DISABLE KEYS */;
INSERT INTO `classe` VALUES (1,'SIO1-SLAM'),(2,'SIO2-SLAM'),(3,'CG1'),(4,'CG2'),(5,'AM1'),(6,'AM2'),(7,'NRC1'),(8,'NRC2'),(9,'SN1'),(10,'SN2'),(11,'SIO1-SISR'),(12,'SIO2-SISR');
/*!40000 ALTER TABLE `classe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entreprise`
--

DROP TABLE IF EXISTS `entreprise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `entreprise` (
  `num_entreprise` int NOT NULL AUTO_INCREMENT,
  `raison_sociale` varchar(128) NOT NULL,
  `nom_contact` varchar(128) DEFAULT NULL,
  `nom_resp` varchar(128) DEFAULT NULL,
  `rue_entreprise` varchar(128) DEFAULT NULL,
  `cp_entreprise` int DEFAULT NULL,
  `ville_entreprise` varchar(128) NOT NULL,
  `tel_entreprise` varchar(32) DEFAULT NULL,
  `fax_entreprise` varchar(32) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `observation` text,
  `site_entreprise` varchar(128) DEFAULT NULL,
  `niveau` varchar(32) NOT NULL,
  `en_activite` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`num_entreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entreprise`
--

LOCK TABLES `entreprise` WRITE;
/*!40000 ALTER TABLE `entreprise` DISABLE KEYS */;
INSERT INTO `entreprise` VALUES (1,'Webzine Maker (Campusplex)','Antoine Dupont','Antoine Dupont','12 Rue Général Fiorella',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.wmaker.net/','BAC+1/BAC+2',1),(2,'DuoApps (Campusplex)','Bernard Jean','Bernard Jean','12 Rue Général Fiorella',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.duoapps.com/','BAC+1/BAC+2',1),(3,'Ollandini','Etienne Jacques','Gilbert Durand','1 Rue Paul Colonna d\'Istria',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.ollandini.fr/','BAC+2',1),(4,'Conseil Départemental de la Corse du Sud','Juliette André','Juliette André','4 Cours Napoléon',20183,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.cg-corsedusud.fr','BAC+1/BAC+2',1),(5,'Communauté d\'Agglomération du Pays Ajaccien (CAPA)','Pierre Paul','Pierre Paul','18 rue Comte de Marbeuf',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.ca-ajaccien.fr/','BAC+1/BAC+2',1),(6,'Centre Hospitalier Miséricorde','Gerard Blanc','','Av Impératrice Eugénie',20303,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','','Bac+1/Bac+2',1),(7,'IPC - Informatique Professionnelle Corse','Elisabeth Dubreuil','Elisabeth Dubreuil','Parc San Lazaro - Av Napoléon III',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.ipc-corse.com','Bac+1/Bac+2',1),(8,'La Poste - Centre financier d\'Ajaccio','Pierre Roger','','22 avenue du Colonel Colonna d\'Ornano',20090,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','','BAC+1/BAC+2',1),(9,'Conseil Départemental de la Corse du Sud','Jacques Antoine','','8 Cours général Leclerc',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.cg-corsedusud.fr','BAC+1',1),(10,'IRA de Bastia','Jean Lurat','Jean Lurat','Quai des Martyrs de la Libération',20200,'Bastia','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','https://www.ira-bastia.fr','BAC+1/BAC+2',1),(11,'ARS - Agence Régionale de la Santé','Marie Gibe','','Route Saint-Joseph',20090,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.ars.corse.sante.fr/Internet.corse.0.html','BAC+1/BAC+2',1),(12,'Lycée Montesoro','Alexandre Aed','','rue de la 4éme DMM',20200,'Bastia','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','','BAC+1/BAC+2',1),(13,'Rectorat de Corse','Paule André','Le Recteur','Bd Pascal Rossini',20192,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.ac-corse.fr','BAC+1/BAC+2',1),(14,'EDF - SEI Centre de Corse','Hugo Milau','','2 Avenue Impératrice Eugénie',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','','BAC+1/BAC+2',1),(15,'France 3 Corse Via Stella','Jean Tele','','8 Rue André Touranjon',20700,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','','BAC+1/BAC+2',1),(16,'CCI  Corse du Sud','Sophie Bato','','Gare maritime',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.2a.cci.fr','BAC+2',1),(17,'Conseil Départemental de la Corse du Sud','Albert Dupont','Edith Robe','8 Cours général Leclerc',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.cg-corsedusud.fr','BAC+2',1),(18,'SARL OCTAEDRA','Julie Resp','Julie Resp','Route du Vazzio pont du Ricanto',20090,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.aria-tourisme.com','BAC+1/BAC+2',1),(19,'ESI-DGFIP d\'Ajaccio','Didier Tresor','','Immeuble Castellani Saint-Joseph',20090,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','','BAC+1/BAC+2',1),(20,'Rocca Transport','Bruno Trans','','ZI Baleone',20501,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.rocca-transports.fr','BAC+1/BAC+2',1),(21,'EDF Corse','Christophe Elec','','2 Avenue Impératrice',20174,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','','BAC+1',1),(22,'PIC Informatique','Paul Pic','Paul Pic','Immeuble LOGOS, Avenue du mont Thabor',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','','BAC+1',1),(23,'Servitec Calvi','Pierre Henriet','','Lieu-Dit Campo Longo, Route de Calenzana',20260,'Calvi','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','','BAC+1',1),(24,'ARS - Agence Régionale de la Santé','Albert Mirou','','Route Saint-Joseph',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.ars.corse.sante.fr/Internet.corse.0.html','BAC+1',1),(25,'Crédit Agricole','Paul Paul','','1 Avenue Napoléon III',20090,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','','BAC+1/BAC+2',1),(26,'Raffalli Travaux Publics','Lucien Turin','','Zone Industrielle Caldaniccia',20167,'Sarrola-Carcopino','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','','BAC+1/BAC+2',1),(27,'Centre Hospitalier de Bastia','Michel Oliver','','Bastia',20600,'Bastia','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.ch-bastia.fr/','BAC+1/BAC+2',1),(28,'CTC - Collectivité Territoriale de Corse','Pierre Valert','','Service Recherche 22 cours Grandval',20187,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.corse.fr','BAC+1',1),(29,'CTC - Collectivité Territoriale de Corse','Lucie Dupond','','Service SIG - 22 cours Grandval',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.corse.fr','BAC+1/BAC+2',1),(30,'CTC - Collectivité Territoriale de Corse','Jean-Pierre Moulin','','DSI - 22 cours Grandval',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.corse.fr','BAC+1/BAC+2',1),(31,'GIRTEC','Louise Map','','28 cours Grandval',20000,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.cg-corsedusud.fr/collectivite-departementale/ses-partenaires/le-girtec/','BAC+1/BAC+2',1),(32,'SAGES Informatique','Alain Ged','','Lieu-dit Pernicaggio, ZA de la Caldaniccia',20167,'Sarrola-Carcopino','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.sages-informatique.com','BAC+1/BAC+2',1),(33,'Nextiraone','Edouard Network','','Chemin de Pietralba',20090,'Ajaccio','01 02 03 04 05','01 02 03 04 05','contact@infos.fr','','http://www.nextiraone.fr/fr/home','BAC+1/BAC+2',1),(44,'Test entreprise','Jean Michel','Jacques Goldman','Rue du test',69001,'Lyon','0875961232','0123456789','test@entreprise.com','','','Bac+1',0),(45,'Test','Test','Test','Test',60999,'Test','0123456789','0123456789','Test@Test.com','TestTestTestTest','Test.com','BAC+2',0);
/*!40000 ALTER TABLE `entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etudiant`
--

DROP TABLE IF EXISTS `etudiant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `etudiant` (
  `num_etudiant` int NOT NULL AUTO_INCREMENT,
  `nom_etudiant` varchar(64) NOT NULL,
  `prenom_etudiant` varchar(64) NOT NULL,
  `login` varchar(8) NOT NULL,
  `mdp` varchar(30) NOT NULL,
  `num_classe` int NOT NULL,
  PRIMARY KEY (`num_etudiant`),
  KEY `num_classe` (`num_classe`),
  CONSTRAINT `etudiant_ibfk_1` FOREIGN KEY (`num_classe`) REFERENCES `classe` (`num_classe`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etudiant`
--

LOCK TABLES `etudiant` WRITE;
/*!40000 ALTER TABLE `etudiant` DISABLE KEYS */;
INSERT INTO `etudiant` VALUES (8,'Bentot','Pascal','benpas01','0e8c9d790c0aecc25b92829bb29a7',1),(11,'Tusseau','Louis','tuslou01','ff297a526557dc47ab49c59a0230d',2),(12,'Finck','Jacques','finjac01','2ba377fe03892ec54fab739aa7223',1),(14,'Andre','David','anddav01','78619b69a34b3f632bf6fc2df1348',2),(15,'Bedos','Christian','bedchr01','671f4ff9fc827b313c76f5ee3ae89',2),(16,'Cadic','Eric','caderi01','df6c5db1a3ac96d60be30110535bc',3),(17,'Desmarquest','Nathalie','desnat01','6525818233b85273dab5b95717204',3),(18,'Desnost','Pierre','despie01','28b9fdffefcd75efe77b407fa9a1c',4),(19,'Eynde','Valérie','eynval01','22af70c923250e4f363ae87fcefb1',4),(20,'Frémont','Fernande','frefer01','80197104b49b2e8d5f7a6d159b67f',5),(21,'Guest','Alain','gueala01','76e1e2977693f30f39f1af7ed518e',5),(22,'Bioret','Luc','bioluc01','5dfc2aa08131a64c9816ed3dd62dd',6),(23,'Bunisset','Denise','bunden01','9b69b213c6e4697b8ba40285bfd05',6),(24,'Bunisset','Francis','bunfra01','02716d610b00960a727935373d473',7),(25,'Villechane','Louis','villou01','f749a6a3b552bacf8672e50c3ced9',7),(26,'De','Philippe','de_phi01','b0309f3372334042f4d92193ff57e',8),(27,'Debelle','Michel','debmic01','633352166aec7356e87be7a2e17ce',8),(28,'Debelle','Jeanne','debjea01','a865b5c26d570b227a0493aebadb4',9),(29,'Cottin','Vincenne','cotvin01','a047023dbb5a366b1af36e84fffda',9),(30,'Charoze','Catherine','chacat01','0e87643acbf37c581bedd2ecb0bd3',10),(31,'Cacheux','Bernard','cacber01','d36a19c5c422b0c581302ceba3356',10),(32,'Dupond','Jean','dupjea01','661154e9ac1dc0fab76fb3e48b697',11),(33,'Durand','Simon','dursim01','08fbcfa861f025584fe652631253b',11),(34,'Leroy','Edouard','leredo01','9edc4b89c2f1f18b180ff1580d042',12),(38,'Labrosse','Lilian','lillab01','0cc4ecf175e5298aa1ec2fba5b3cd',1);
/*!40000 ALTER TABLE `etudiant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prof_classe`
--

DROP TABLE IF EXISTS `prof_classe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prof_classe` (
  `num_prof` int NOT NULL,
  `num_classe` int NOT NULL,
  `est_prof_principal` tinyint(1) NOT NULL,
  PRIMARY KEY (`num_prof`,`num_classe`),
  KEY `num_classe` (`num_classe`),
  CONSTRAINT `prof_classe_ibfk_1` FOREIGN KEY (`num_prof`) REFERENCES `professeur` (`num_prof`),
  CONSTRAINT `prof_classe_ibfk_2` FOREIGN KEY (`num_classe`) REFERENCES `classe` (`num_classe`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prof_classe`
--

LOCK TABLES `prof_classe` WRITE;
/*!40000 ALTER TABLE `prof_classe` DISABLE KEYS */;
INSERT INTO `prof_classe` VALUES (1,1,1),(1,2,0),(2,1,0),(2,2,1),(3,1,0),(3,2,0),(5,3,1),(5,4,0),(6,3,0),(6,4,1),(7,3,0),(7,4,0),(8,5,1),(8,6,0),(9,5,0),(9,6,1),(10,5,0),(10,6,0),(11,7,1),(11,8,0),(12,7,0),(12,8,1),(13,7,0),(13,8,0),(14,9,1),(14,10,0),(15,9,0),(15,10,1),(16,9,0),(16,10,0);
/*!40000 ALTER TABLE `prof_classe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professeur`
--

DROP TABLE IF EXISTS `professeur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `professeur` (
  `num_prof` int NOT NULL AUTO_INCREMENT,
  `nom_prof` varchar(64) NOT NULL,
  `prenom_prof` varchar(64) NOT NULL,
  `login` varchar(8) NOT NULL,
  `mdp` varchar(8) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`num_prof`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professeur`
--

LOCK TABLES `professeur` WRITE;
/*!40000 ALTER TABLE `professeur` DISABLE KEYS */;
INSERT INTO `professeur` VALUES (1,'Shavert','Anne-Lucie','shaann01','shaann01','shaann01@llb.fr'),(2,'Gaudin','Jules','gaujul01','gaujul01','gaujul01@llb.fr'),(3,'Zerbib','Gilles','zergil01','zergil01','zergil01@llb.fr'),(5,'Di Conota','Prosper','di_pro01','di_pro01','di_pro01@llb.fr'),(6,'Ferdinand','Anne-Lucie','ferann01','ferann01','ferann01@llb.fr'),(7,'Chamois','Andrew','chaand01','chaand01','chaand01@llb.fr'),(8,'Lirevien','John','lirjoh01','lirjoh01','lirjoh01@llb.fr'),(9,'Fortin','François','forfra01','forfra01','forfra01@llb.fr'),(10,'Segura','Irénée','segire01','segire01','segire01@llb.fr'),(11,'Pistache','Christophe','pischr01','pischr01','pischr01@llb.fr'),(12,'Cherioux','Aurèle','cheaur01','cheaur01','cheaur01@llb.fr'),(13,'Certifat','Alice','cerali01','cerali01','cerali01@llb.fr'),(14,'Pastor','Camille','pascam01','pascam01','pascam01@llb.fr'),(15,'Hansbern','Johnny','hanjoh01','hanjoh01','hanjoh01@llb.fr'),(16,'Billahian','Andrée','biland01','biland01','biland01@llb.fr'),(17,'Merlot','Merlin','merlot','secret','merlot@llb.fr');
/*!40000 ALTER TABLE `professeur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spec_entreprise`
--

DROP TABLE IF EXISTS `spec_entreprise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spec_entreprise` (
  `num_entreprise` int NOT NULL,
  `num_spec` int NOT NULL,
  PRIMARY KEY (`num_entreprise`,`num_spec`),
  KEY `num_spec` (`num_spec`),
  CONSTRAINT `spec_entreprise_ibfk_1` FOREIGN KEY (`num_entreprise`) REFERENCES `entreprise` (`num_entreprise`),
  CONSTRAINT `spec_entreprise_ibfk_2` FOREIGN KEY (`num_spec`) REFERENCES `specialite` (`num_spec`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spec_entreprise`
--

LOCK TABLES `spec_entreprise` WRITE;
/*!40000 ALTER TABLE `spec_entreprise` DISABLE KEYS */;
INSERT INTO `spec_entreprise` VALUES (2,1),(4,1),(15,1),(16,1),(18,1),(19,1),(22,1),(24,1),(26,1),(29,1),(31,1),(32,1),(1,2),(3,2),(5,2),(6,2),(7,2),(8,2),(9,2),(10,2),(11,2),(12,2),(13,2),(14,2),(17,2),(20,2),(21,2),(23,2),(25,2),(27,2),(28,2),(30,2),(33,2),(45,3);
/*!40000 ALTER TABLE `spec_entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specialite`
--

DROP TABLE IF EXISTS `specialite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `specialite` (
  `num_spec` int NOT NULL AUTO_INCREMENT,
  `libelle` varchar(128) NOT NULL,
  PRIMARY KEY (`num_spec`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specialite`
--

LOCK TABLES `specialite` WRITE;
/*!40000 ALTER TABLE `specialite` DISABLE KEYS */;
INSERT INTO `specialite` VALUES (1,'SLAM'),(2,'SISR'),(3,'AM'),(4,'SN'),(5,'CG'),(6,'NRC');
/*!40000 ALTER TABLE `specialite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stage`
--

DROP TABLE IF EXISTS `stage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stage` (
  `num_stage` int NOT NULL AUTO_INCREMENT,
  `debut_stage` date DEFAULT NULL,
  `fin_stage` date DEFAULT NULL,
  `type_stage` varchar(128) DEFAULT NULL,
  `desc_projet` text,
  `observation_stage` text,
  `num_etudiant` int NOT NULL,
  `num_prof` int NOT NULL,
  `num_entreprise` int NOT NULL,
  PRIMARY KEY (`num_stage`),
  KEY `num_etudiant` (`num_etudiant`),
  KEY `num_prof` (`num_prof`),
  KEY `num_entreprise` (`num_entreprise`),
  CONSTRAINT `stage_ibfk_1` FOREIGN KEY (`num_etudiant`) REFERENCES `etudiant` (`num_etudiant`),
  CONSTRAINT `stage_ibfk_2` FOREIGN KEY (`num_prof`) REFERENCES `professeur` (`num_prof`),
  CONSTRAINT `stage_ibfk_3` FOREIGN KEY (`num_entreprise`) REFERENCES `entreprise` (`num_entreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stage`
--

LOCK TABLES `stage` WRITE;
/*!40000 ALTER TABLE `stage` DISABLE KEYS */;
INSERT INTO `stage` VALUES (1,'2015-07-13','2016-08-31','alternance','Apprentissage sur 1 ans.</br>\r\nParticipation aux différents projets de l\'entreprise.</br>\r\nDéveloppement et maintenance de plugins.','',8,2,1),(3,'2016-01-11','2016-02-19','stage','Mise à jour de la documentation des applications métiers de l\'entreprise.\r\nDéveloppement d\'un module d\'authentification.','',11,1,4),(5,'2017-01-08','2017-02-17','stage','Evolution de l\'application de gestion de projets utilisée par l\'entreprise.<br/>\r\nParticipation à l\'assistance utilisateur pour les divers logiciels développés en interne.',NULL,11,2,5),(8,'2023-02-07','2023-03-18','Stage','Petit stage de café','',8,6,1),(9,'2023-02-06','2023-04-13','Alternance','','',18,9,15),(10,'2023-02-12','2023-08-12','Stage','Utilisation de Spring Boot et Thymeleaf','',22,14,14),(11,'2023-02-01','2023-03-30','Stage','Découverte de flutter','',14,7,28);
/*!40000 ALTER TABLE `stage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mission`
--

DROP TABLE IF EXISTS `mission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mission` (
                           `num_mission` int NOT NULL AUTO_INCREMENT,
                           `libelle` varchar(128) NOT NULL,
                           `num_stage` int NOT NULL,
                           PRIMARY KEY (`num_mission`),
                           KEY `num_stage` (`num_stage`),
                           CONSTRAINT `mission_ibfk_1` FOREIGN KEY (`num_stage`) REFERENCES `stage` (`num_stage`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mission`
--

LOCK TABLES `mission` WRITE;
/*!40000 ALTER TABLE `mission` DISABLE KEYS */;
INSERT INTO `mission` VALUES (1,'Découverte de l\'entreprise',1),(2,'Prise en main de l\'outil de versioning',1),(3,'Développement d\'un plugin de type Wordpress pour le CMS de l\'entreprise',1),(4,'Découverte de l\'entreprise',3),(5,'Analyse et mise à jour de la documentation technique de l\'application \"appProj\"',3),(6,'Découverte de l\'entreprise',5),(7,'Prise en main de l\'API \"comJSON\"',5);
/*!40000 ALTER TABLE `mission` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-12 16:12:45
